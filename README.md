Paper repository for 

# A PERFORMANCE PORTABLE, FULLY IMPLICIT LANDAU COLLISION OPERATOR WITH BATCHED LINEAR SOLVERS

Mark F. Adams, Peng Wang, Jacob Merson, Kevin Huck, Matthew G. Knepley

Portable solvers for batches of many small systems applied to the Landau collision operator
A performance portable, fully implicit Landau collision operator with batched linear solvers
https://arxiv.org/abs/2209.03228

mfadams@lbl.gov

## Data files

The test data files for the "throughput" section (4) are in the
**landau\_throughput\_nsight** directory, with the _Crusher_ data in the
**crusher** directory and _Perlmutter_ data in the **perlmutter**
directory.
The test data files for the "anisotropic" section (7) are in the
**landau\_anisotropic\_shift directory**.
Each experiment directory has a readme.md that shows how to reproduce
the plots and tables in the paper (eg, **./plot.py out\***)

Each directory has the data files, a readme file with the command to
generate the plots, which includes the listing of the data files, and
sample batch scripts to run the code.

## Building PETSc

Use git to build PETSc or use any version above v3.19.2 if you prefer.
The code is stable and performance should be identical.

> git clone https://gitlab.com/petsc/petsc.git; cd petsc; export PETSC_DIR=$PWD

Each data file has the PETSc version on a line such as:
 
Using Petsc Development GIT revision: v3.19.2-436-g73a35e49648  GIT Date: 2023-06-12 15:42:20 -0400

which can be retrieved with 

> git checkout v3.19.2

Then configure and
build PETSc as usual (https://petsc.org/release/install/).

## build the executable

Then build the executable from the PETSc root directory, for example
the for the anisotropic data (7):

> cd src/ts/utils/dmplexlandau/tutorials ; make ex1

and for the throughput data (4):

> make ex2

## Running tests

Each data file (files that start with "out", have a listing of the
options used at the end of the file, such as:

#PETSc Option Table entries (each option on separate line as in the
data file):

-dim 3
-dm_landau_amr_levels_max 2,2,2
-dm_landau_batch_size 32
 ....
  
 These lines can be put in a .petscrc file in the program working
 directory and run the executable ./ex[1|2] with one MPI process.
 
 Each data directory has example batch scripts for running the jobs and
 corespond to how the data was generated.

## NVIDIA hardware utilization Nsight Compute  data (section 8.1):

**landau\_throughput\_nsight/perlmutter/nsight\_compute**
