
## Running jobs

Batch scripts that produced the data that provided data files, eg,
". run_crusher2.sbatch", that can be edited for each case (eg, QQ=2
for second order elements)

# Plotting

Plots can be generated with, for example:
 ./plot.py out-aniso-maxwellians-0-P?-\*AMD\*-filtered
 ./plot.py out-aniso-maxwellians-0-P?-\*NVIDIA\*-filtered
 ./plot.py out-aniso-maxwellians-0-Q?-\*NVIDIA\*Quad+Semicircle-filtered
