#!/bin/bash -x
#SBATCH -A m1489_g
#SBATCH -C gpu
#SBATCH -J landau-Q
#SBATCH -q regular
#SBATCH -t 2:00:00
#SBATCH --ntasks-per-node=4
#SBATCH -c 32
#SBATCH --gpus-per-task=1
#SBATCH -N 1

date

RAD=5
PP=0
TOL=6
GRID=Quad+Semicircle
RERAD=0

#NG2=1
#DT2=1e-4
#MAXDT2=0.05
#TIME=363.9
NG2=0
DT2=1e-3
MAXDT2=1
#TIME=100
TIME=14000

#need to adjust AMR and ORDER for plot names to be correct
QQ=2
AMR=2
AMR2=${AMR}
REXTRA='-print_nrl true -dm_landau_sphere_inner_radius_45degree_scale .7 -dm_landau_sphere_inner_radius_90degree_scale .65 -dm_landau_amr_levels_max 1,1 -petscspace_degree 3'
#REXTRA='-print_nrl true -dm_landau_sphere_inner_radius_45degree_scale .45 -dm_landau_sphere_inner_radius_90degree_scale .4 -dm_landau_amr_levels_max 0,0 -petscspace_degree 4'
REXTRA='-print_nrl true -dm_landau_sphere_inner_radius_45degree_scale .7 -dm_landau_sphere_inner_radius_90degree_scale .65 -dm_landau_amr_levels_max 2,2 -petscspace_degree 2'

#Hardwire for Perlmutter
export TFILE=out-aniso-maxwellians-${NG2}-Q${QQ}-adaptive_tol${TOL}-P${PP}_AMR${AMR}-4_NVIDIA_A100_GPUs-${GRID}

#make -f mymake runex1-3d PETSC_DIR=/Users/markadams/Codes/petsc PETSC_ARCH=arch-macosx-gnu-O DEVICE=kokkos SOLVER=batch-tfqmr DT=${DT2} AMR=${AMR} AMR2=${AMR2} RAD=${RAD} RERAD=${RERAD} POST=${PP} ORDER=${QQ} NG=${NG2} MAXDT=${MAXDT2} NP=1 STEPS=1000 TIME=${TIME} EX_PATH=~/Codes/petsc/src/ts/utils/dmplexlandau/tutorials EXTRA="-options_left -ts_rtol 1e-${TOL} ${REXTRA} -dm_landau_num_cells ${GRID}" KSPRTOL=1e-6 BSZ=1
make -f mymake runex1-quad-semi DEVICE=kokkos SOLVER=batch-tfqmr DT=${DT2} AMR=${AMR} AMR2=${AMR2} RAD=${RAD} RERAD=${RERAD} POST=${PP} MZ=${MZ} ORDER=${QQ} NG=${NG2} MAXDT=${MAXDT2} NP=1 STEPS=100000 TIME=${TIME} EX_PATH=/global/u2/m/madams/petsc/src/ts/utils/dmplexlandau/tutorials EXTRA="-options_left -log_view -log_view_gpu_time -ts_rtol 1e-${TOL} ${REXTRA}" KSPRTOL=1e-6 BSZ=256
date

echo ./plot.py ${TFILE}

#make -f mymake filter PETSC_ARCH=arch-olcf-crusher
#date
