#!/usr/bin/env python3
#
# plot out11_* where out11_2D_008_Batch-Kokkos_Tfqmr_Perlmutter_1_NVIDIA-A100.txt: batch size 4, KK GMRES, on Perlmutter, 1 GPU, NVIDIA-A100 GPU
#
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd
import seaborn as sns

krylov2 = 'dummy'
fields = np.zeros( (2,2,11,12) ) # dim idx ; solver  ; batch index ; fields (Jac,mass,ksp,solve,its,N,n-ints,TP(Newt/s),TP(solves/sec)
nfiles = 0
max_bid = 0
max_solve_idx = 0
for filename in sys.argv[1:]:
    print (filename)
    base = filename.split('.')
    words = base[0].split('_')
    nfiles += 1
    row = int(words[1][0]) - 2
    solver = words[3]
    krylov = words[4]
    machine = words[5] # static
    GPU = words[7]     # static
    nGPU = float(words[6]) # n GPU static
    bsz = float(words[2]) # n batch
    bidx = int(np.log2(bsz))
    if bidx > max_bid: max_bid = bidx
    if krylov == 'GMRES': solve_idx = 2
    elif solver == 'Ensemble-PETSc': solve_idx = 1 ; krylov2 = krylov
    else : solve_idx = 0
    if solve_idx > max_solve_idx: max_solve_idx = solve_idx
    #print ('row=', row, ' bsz=', bsz , ' solve_idx=', solve_idx, ' bidx = ', bidx, solver, krylov, machine, GPU)
    did_nl=0
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if n > 1 and words[1] == 'FormLandau' :
            ncells = words[4]
            stri = words[15]
            words2 = stri.split('=')
            fields[row,solve_idx,bidx,5] = int(words2[1]) # N
        elif n > 4 and words[0] == 'Landau' and words[1] == 'Jacobian' :
            fields[row,solve_idx,bidx,0] = float(words[4])
        elif n > 4 and words[0] == 'Landau' and words[1] == 'Mass' :
            fields[row,solve_idx,bidx,1] = float(words[4])
        elif n > 1 and words[1] == 'Solve:':
            fields[row,solve_idx,bidx,3] = float(words[2])
        elif n > 2 and words[0] == 'KSPSolve':
            fields[row,solve_idx,bidx,2] = float(words[3])
        elif n > 5 and words[3] == 'nonlinear':
            stri = words[5]
            words2 = stri.split('=')
            if words2[0] != 'failures': fields[row,solve_idx,bidx,6] = float(words2[1]) # N newt
            did_nl = 0
        elif n > 5 and words[3] == 'linear' and did_nl==1:
            stri = words[5]
            words2 = stri.split('=')
            fields[row,solve_idx,bidx,4] = int(words2[1]) # N lin its
        did_nl = did_nl + 1
    fields[row,solve_idx,bidx,7] = fields[row,solve_idx,bidx,6]*bsz*nGPU/fields[row,solve_idx,bidx,0] # TP ~#Jac/sec
    fields[row,solve_idx,bidx,8] = fields[row,solve_idx,bidx,6]*bsz*nGPU/fields[row,solve_idx,bidx,1] # TP ~#Mass/sec
    fields[row,solve_idx,bidx,9] = fields[row,solve_idx,bidx,6]*bsz*nGPU/fields[row,solve_idx,bidx,2] # TP ~#KSP/sec
    fields[row,solve_idx,bidx,10]= fields[row,solve_idx,bidx,6]*bsz*nGPU/fields[row,solve_idx,bidx,3] # TP #Newt/sec
    fields[row,solve_idx,bidx,11]= fields[row,solve_idx,bidx,4]*bsz*nGPU/fields[row,solve_idx,bidx,3] # TP #KSP it/KSP time
if nfiles == 0: print('no files: Usage plot.py out*')
#             0    1    2    3    4     5     6     7      8     9      10
num_batch = ['1', '2', '4', '8', '16', '32', '64', '128', '256', '512', '1024']
dim_strs = ['2V', '3V']
solve_names = ['Batch ' + krylov2,'Aggregated ' + krylov2,'Batch GMRES']
type_tag = [krylov2.lower(),'ensemble','gmres']
part_names = ['Jacobian', 'Mass', 'Solve', 'Total', 'Krylov its']
print ('\n')
#            print ('       np=', np, ', num=', nn, ' num tot = ', np*nn, ' time=',solve_time[be_idx][smp_idx][np_idx],' rate=', newt_sec[be_idx][smp_idx][np_idx])
pd.options.display.float_format = '{:,}'.format
#
#print(fields[:,1,:max_bid+1,9])
for solve_idx in range(max_solve_idx+1):
    solver = solve_names[solve_idx]
#
    dfc = pd.DataFrame(fields[:,solve_idx,:max_bid+1,9], index=dim_strs, columns=num_batch[:max_bid+1])
    dfc.columns.name = 'Batch size'

    # HEAT
    styler = dfc.style.background_gradient(axis=1,cmap='Purples',low=.1,high=.1).format(precision=0, thousands=",")
    print(styler.to_latex(convert_css=True,position='h!',hrules=True,caption='Solves / sec, ' + solver + ', ' + str(int(nGPU)) + ' ' + GPU, label='tab:' + type_tag[solve_idx] + '-' + machine + '-solve_throughput'))

    dfc[dfc.eq(0)] = ''
    print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='Solves / sec, ' + solver + ', ' + str(int(nGPU)) + ' ' + GPU, label='tab:' + type_tag[solve_idx] + '-' + machine + '-solve_throughput'))

 
    dfc = pd.DataFrame(fields[:,solve_idx,:max_bid+1,10], index=dim_strs, columns=num_batch[:max_bid+1])
    dfc.columns.name = 'Batch size'

    # HEAT
    styler = dfc.style.background_gradient(axis=1,cmap='Purples',low=.1,high=.1).format(precision=0, thousands=",")
    print(styler.to_latex(convert_css=True,position='h!',hrules=True, caption='Newton iterations / sec, ' + solver + ', ' + str(int(nGPU)) + ' ' + GPU, label='tab:' + type_tag[solve_idx] + '-' + machine + '-Newton-throughput'))

    dfc[dfc.eq(0)] = ''
    print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='Newton iterations / sec, ' + solver + ', ' + str(int(nGPU)) + ' ' + GPU, label='tab:' + type_tag[solve_idx] + '-' + machine + '-Newton-throughput'))

    #d2 = fields[0,solve_idx,8,:5]
    #print(d2)
    #d3 = fields[1,solve_idx,5,:5]
    #print(d3)
    #data2 = np.vstack((d2,d3))
    #print(data2)
    #dfp = pd.DataFrame(data2, index=dim_strs, columns=part_names)
    #dfp.columns.name = 'Component'
    #dfp['Krylov iterations'] = dfp['Krylov iterations'].map('{:,.0f}'.format)
    #print (dfp.to_latex(longtable=False, escape=False, float_format="{:,.2f}".format, caption='Component times (largest case) '  + solver + ', ' + GPU, label='tab:parts'+ type_tag[solve_idx]))
    #
start=3
stop=max_bid+1
last_bids = [max_bid,max_bid-3]
sns.set(font_scale=1.5)
for d_idx in range(2): # DIM
    dim_str = dim_strs[d_idx]

    fig, ax = plt.subplots()
    ax = sns.heatmap(fields[d_idx,:max_solve_idx+1,start:stop,7].transpose(), linewidth=0.5,xticklabels=solve_names[:max_solve_idx+1], yticklabels=num_batch[start:stop], cmap="coolwarm", annot=True, fmt=".3g")
    ax.set_title('Jacobians / sec, ' + dim_str + ', ' + str(int(nGPU)) + ' ' + GPU)
    ax.set_ylabel('Batch size')
    ax.set_xlabel('Solver')
    ax.set_xticklabels(ax.get_xmajorticklabels(), fontsize = 12)
    plt.xticks(rotation=0) 
    #plt.show()
    fig.savefig(machine + '_jac_s_' + dim_str +  '_heatmap.png')

    fig2, ax2 = plt.subplots()
    ax2 = sns.heatmap(fields[d_idx,:max_solve_idx+1,start:stop,8].transpose(), linewidth=0.5,xticklabels=solve_names[:max_solve_idx+1], yticklabels=num_batch[start:stop], cmap="coolwarm", annot=True, fmt=".3g")
    ax2.set_title('Mass / sec, ' + dim_str + ', ' + str(int(nGPU)) + ' ' + GPU)
    ax2.set_ylabel('Batch size')
    ax2.set_xlabel('Solver')
    ax2.set_xticklabels(ax2.get_xmajorticklabels(), fontsize = 12)
    plt.xticks(rotation=0) 
    #plt.show()
    fig2.savefig(machine + '_mass_s_' + dim_str +  '_heatmap.png')

    fig3, ax3 = plt.subplots()
    ax3 = sns.heatmap(fields[d_idx,:max_solve_idx+1,start:stop,9].transpose(), linewidth=0.5,xticklabels=solve_names[:max_solve_idx+1], yticklabels=num_batch[start:stop], cmap="coolwarm", annot=True, fmt=".3g")
    ax3.set_title('Solves / sec, ' + dim_str + ', ' + str(int(nGPU)) + ' ' + GPU)
    ax3.set_ylabel('Batch size')
    ax3.set_xlabel('Solver')
    ax3.set_xticklabels(ax3.get_xmajorticklabels(), fontsize = 12)
    plt.xticks(rotation=0) 
    #plt.show()
    fig3.savefig(machine + '_solves_s_' + dim_str +  '_heatmap.png')

    fig4, ax4 = plt.subplots()
    ax4 = sns.heatmap(fields[d_idx,:max_solve_idx+1,start:stop,10].transpose(), linewidth=0.5,xticklabels=solve_names[:max_solve_idx+1], yticklabels=num_batch[start:stop], cmap="coolwarm", annot=True, fmt=".3g")
    ax4.set_title('Newton / sec, ' + dim_str + ', ' + str(int(nGPU)) + ' ' + GPU)
    ax4.set_ylabel('Batch size')
    ax4.set_xlabel('Solver')
    ax4.set_xticklabels(ax4.get_xmajorticklabels(), fontsize = 12)
    plt.xticks(rotation=0) 
    #plt.show()
    fig4.savefig(machine + '_newt_s_' + dim_str +  '_heatmap.png')

    fig3, ax3 = plt.subplots()
    ax3 = sns.heatmap(fields[d_idx,:max_solve_idx+1,start:stop,11].transpose(), linewidth=0.5,xticklabels=solve_names[:max_solve_idx+1], yticklabels=num_batch[start:stop], cmap="coolwarm", annot=True, fmt=".3g")
    ax3.set_title('KSP iterations / sec, ' + dim_str + ', ' + str(int(nGPU)) + ' ' + GPU)
    ax3.set_ylabel('Batch size')
    ax3.set_xlabel('Solver')
    ax3.set_xticklabels(ax3.get_xmajorticklabels(), fontsize = 12)
    plt.xticks(rotation=0) 
    #plt.show()
    fig3.savefig(machine + '_it_s_' + dim_str +  '_heatmap.png')

    dfp = pd.DataFrame(fields[d_idx,:max_solve_idx+1,last_bids[d_idx],:5], index=solve_names[:max_solve_idx+1], columns=part_names)
    dfp.columns.name = 'Component'
    dfp['Krylov its'] = dfp['Krylov its'].map('{:,.0f}'.format)
    print (dfp.to_latex(longtable=False, escape=False, float_format="{:,.2f}".format, caption=dim_str + ' Component times (batch size = ' + num_batch[last_bids[d_idx]] + '), ' + GPU, label='tab:parts-' + machine + '-' + dim_str))

    start=0
    stop=max_bid + 1 - 3

solve_idx = 1
dfc = pd.DataFrame(fields[:,solve_idx,:,5], index=dim_strs, columns=num_batch)
dfc.columns.name = 'Batch size'
dfc[dfc.eq(0)] = ''
print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='Matrix size N', label='tab:N'))
