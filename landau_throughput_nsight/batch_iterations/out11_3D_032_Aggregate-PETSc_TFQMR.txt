masses:        e= 9.109e-31; ions in proton mass units:    2.000e+00  1.820e+02 ...
charges:       e=-1.602e-19; charges in elementary units:  1.000e+00  2.000e+00
n:             e:  1.000e+00                           i:  1.000e+00  1.000e-07
thermal T (K): e= 2.321e+07 i= 1.160e+07  1.160e+07. Normalization grid 0: v_0= 1.876e+07 ( 6.256e-02c) n_0= 1.000e+20 t_0= 1.909e-05 32 batched, view batch 0
Domain radius (AMR levels) grid 0: par= 5.000e+00 perp= 5.000e+00 (2) , 1: par= 5.835e-02 perp= 5.835e-02 (2) , 2: par= 6.117e-03 perp= 6.117e-03 (2) 
0) FormLandau 9720 IPs, 360 cells total, Nb=27, Nq=27, dim=3, Tab: Nb=27 Nf=10 Np=27 cdim=3 N=334400
0 TS dt 0.5 time 0.
    0 SNES Function norm 1.165212582001e-01
      Linear solve converged due to CONVERGED_RTOL iterations 17
    1 SNES Function norm 3.941469416799e-03
      Linear solve converged due to CONVERGED_RTOL iterations 16
    2 SNES Function norm 7.255565470493e-04
      Linear solve converged due to CONVERGED_RTOL iterations 17
    3 SNES Function norm 1.652860447803e-04
      Linear solve converged due to CONVERGED_RTOL iterations 15
    4 SNES Function norm 3.893751351294e-05
      Linear solve converged due to CONVERGED_RTOL iterations 14
    5 SNES Function norm 9.268721343136e-06
      Linear solve converged due to CONVERGED_RTOL iterations 16
    6 SNES Function norm 2.215621269931e-06
      Linear solve converged due to CONVERGED_RTOL iterations 16
    7 SNES Function norm 5.307052179922e-07
      Linear solve converged due to CONVERGED_RTOL iterations 17
    8 SNES Function norm 1.272685462123e-07
      Linear solve converged due to CONVERGED_RTOL iterations 17
    9 SNES Function norm 3.054530759690e-08
      Linear solve converged due to CONVERGED_RTOL iterations 17
   10 SNES Function norm 7.335890189891e-09
      Linear solve converged due to CONVERGED_RTOL iterations 17
   11 SNES Function norm 1.762854195369e-09
      Linear solve converged due to CONVERGED_RTOL iterations 17
   12 SNES Function norm 4.238574588254e-10
      Linear solve converged due to CONVERGED_RTOL iterations 17
   13 SNES Function norm 1.019653710839e-10
      Linear solve converged due to CONVERGED_RTOL iterations 18
   14 SNES Function norm 2.454182176129e-11
      Linear solve converged due to CONVERGED_RTOL iterations 15
   15 SNES Function norm 5.910041413127e-12
      Linear solve converged due to CONVERGED_RTOL iterations 20
   16 SNES Function norm 1.424108633869e-12
    Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 16
      TSAdapt none beuler 0: step   0 accepted t=0          + 5.000e-01 dt=5.000e-01 
1 TS dt 0.5 time 0.5
TS Object: 1 MPI process
  type: beuler
  maximum steps=1
  maximum time=100.
  total number of I function evaluations=17
  total number of I Jacobian evaluations=16
  total number of RHS function evaluations=17
  total number of nonlinear solver iterations=16
  total number of linear solver iterations=266
  total number of nonlinear solve failures=0
  total number of rejected steps=0
  using relative error tolerance of 0.01,   using absolute error tolerance of 0.0001
  TSAdapt Object: 1 MPI process
    type: none
  SNES Object: 1 MPI process
    type: newtonls
    maximum iterations=50, maximum function evaluations=10000
    tolerances: relative=1e-14, absolute=1e-50, solution=1e-14
    total number of linear solver iterations=266
    total number of function evaluations=17
    norm schedule ALWAYS
    SNESLineSearch Object: 1 MPI process
      type: basic
      maxstep=1.000000e+08, minlambda=1.000000e-12
      tolerances: relative=1.000000e-08, absolute=1.000000e-15, lambda=1.000000e-08
      maximum iterations=40
    KSP Object: 1 MPI process
      type: tfqmr
      maximum iterations=100, initial guess is zero
      tolerances: relative=1e-08, absolute=1e-50, divergence=10000.
      left preconditioning
      using PRECONDITIONED norm type for convergence test
    PC Object: 1 MPI process
      type: jacobi
        type DIAGONAL
      linear system matrix = precond matrix:
      Mat Object: Jacobian 1 MPI process
        type: seqaijkokkos
        rows=334400, cols=334400
        total: nonzeros=18048320, allocated nonzeros=18048320
        total number of mallocs used during MatSetValues calls=0
          not using I-node routines
0 TS dt 0.5 time 0.
TS Object: 1 MPI process
  type: beuler
  maximum steps=0
  maximum time=100.
  total number of I function evaluations=17
  total number of I Jacobian evaluations=16
  total number of RHS function evaluations=17
  total number of nonlinear solver iterations=0
  total number of linear solver iterations=0
  total number of nonlinear solve failures=0
  total number of rejected steps=0
  using relative error tolerance of 0.01,   using absolute error tolerance of 0.0001
  TSAdapt Object: 1 MPI process
    type: none
  SNES Object: 1 MPI process
    type: newtonls
    maximum iterations=50, maximum function evaluations=10000
    tolerances: relative=1e-14, absolute=1e-50, solution=1e-14
    total number of linear solver iterations=266
    total number of function evaluations=17
    norm schedule ALWAYS
    SNESLineSearch Object: 1 MPI process
      type: basic
      maxstep=1.000000e+08, minlambda=1.000000e-12
      tolerances: relative=1.000000e-08, absolute=1.000000e-15, lambda=1.000000e-08
      maximum iterations=40
    KSP Object: 1 MPI process
      type: tfqmr
      maximum iterations=100, initial guess is zero
      tolerances: relative=1e-08, absolute=1e-50, divergence=10000.
      left preconditioning
      using PRECONDITIONED norm type for convergence test
    PC Object: 1 MPI process
      type: jacobi
        type DIAGONAL
      linear system matrix = precond matrix:
      Mat Object: Jacobian 1 MPI process
        type: seqaijkokkos
        rows=334400, cols=334400
        total: nonzeros=18048320, allocated nonzeros=18048320
        total number of mallocs used during MatSetValues calls=0
          not using I-node routines
WARNING! There are options you set that were not used!
WARNING! could be spelling mistake, etc!
There is one unused database option. It is:
Option left: name:-dm_landau_coo_assembly (no value) source: file
