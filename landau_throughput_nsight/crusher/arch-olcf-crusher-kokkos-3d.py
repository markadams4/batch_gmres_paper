#!/usr/bin/python3

#Currently Loaded Modules:
#  1) craype-x86-trento    3) craype-network-ofi                     5) cray-pmi/6.1.2        7) amd/5.1.0       9) cray-dsmml/0.2.2   11) cray-libsci/21.08.1.2  13) xalt/1.3.0       15) cmake/3.22.2  17) zlib/1.2.11
#  2) libfabric/1.15.0.0   4) xpmem/2.3.2-2.2_7.8__g93dd7ee.shasta   6) cray-pmi-lib/6.0.17   8) craype/2.7.15  10) cray-mpich/8.1.16  12) PrgEnv-amd/8.3.3       14) DefApps/default  16) emacs/27.2

if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-cc=cc',
    '--with-cxx=CC',
    '--with-fc=ftn',
    #'--with-fortran-bindings=0',
    'LIBS=-L{x}/gtl/lib -lmpi_gtl_hsa'.format(x=os.environ['CRAY_MPICH_ROOTDIR']),
    '--with-debugging=0',
    '--COPTFLAGS=-g -ggdb -O',
    '--CXXOPTFLAGS=-g -ggdb -O',
    '--FOPTFLAGS=-g',
    #'--with-dmlandau-2d',
    #'--with-64-bit-indices=1',
    '--with-mpiexec=srun --gpu-bind=closest',
    '--with-hip',
    '--with-hipc=hipcc',
    #'--download-hypre',
    #'--download-hypre-commit=HEAD',
    #'--download-hypre-configure-arguments=--enable-unified-memory',
    #'--with-hypre-gpuarch=gfx90a',
    #'--with-hip-arch=gfx90a',
    '--download-kokkos',
    '--download-kokkos-kernels',
    '--with-kokkos-kernels-tpl=1',
    '--download-kokkos-kernels-commit=origin/develop',
    '--download-p4est=1',
    '--with-zlib-dir='+os.environ['OLCF_ZLIB_ROOT'],
    #'--prefix=/gpfs/alpine/world-shared/geo127/petsc/arch-crusher-opt-amd',
    #'--prefix=/gpfs/alpine/world-shared/geo127/petsc/arch-crusher-opt-cray-int64
  ]
  configure.petsc_configure(configure_options)

