## Machine directories
A directory for each machine ('perlmutter' - NVIDIA A100, 'crusher' - AMG MI250X) contains the configuration and runs script:

## Running jobs

A batch script that produced that provided data files (out11\_\*) are provided for each machine

# Plotting

In each machine directory the plots can be generated with: ./plot.py out11\_\*

## Raw hardware utilization data with Nsight Compute for NVIDIA is located 

landau\_throughput\_nsight/perlmutter/nsight\_compute
