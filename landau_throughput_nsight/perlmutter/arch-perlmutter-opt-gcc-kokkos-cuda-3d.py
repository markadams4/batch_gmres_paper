#!/usr/bin/python3

#Currently Loaded Modules:
#  1) craype-x86-milan         5) xpmem/2.3.2-2.2_7.5__g93dd7ee.shasta   9) cmake/3.22.0                 13) cray-mpich/8.1.15        17) Nsight-Systems/2022.2.1
#  2) libfabric/1.11.0.4.114   6) gcc/11.2.0                            10) python/3.9-anaconda-2021.11  14) cray-libsci/21.08.1.2    18) cudatoolkit/11.5
#  3) craype-network-ofi       7) xalt/2.10.2                           11) craype/2.7.15                15) PrgEnv-gnu/8.3.3         19) craype-accel-nvidia80
#  4) perftools-base/22.04.0   8) darshan/3.3.1                         12) cray-dsmml/0.2.2             16) Nsight-Compute/2022.1.1

if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--CFLAGS=   -g -ggdb',
    '--CXXFLAGS= -g -ggdb',
    '--CUDAFLAGS=-g -Xcompiler -rdynamic',
    '--LIBS=-lnvToolsExt',
    '--with-cc=cc',
    '--with-cxx=CC',
    '--with-fc=ftn',
    '--with-fortran-bindings=0',    
    '--LDFLAGS=-lmpifort_gnu_91',
#    '--FFLAGS=   -g ',
    '--COPTFLAGS=   -O3',
    '--CXXOPTFLAGS= -O3',
    '--FOPTFLAGS=   -O3',
    '--with-debugging=0',
    '--with-x=0',
    '--with-cuda=1',
    '--with-cuda-arch=80',
    '--with-mpiexec=srun',
    '--with-batch=0',
    '--download-p4est=1',
    '--with-zlib=1',
    '--download-kokkos',
    '--download-kokkos-kernels',
    '--with-kokkos-kernels-tpl=0',
#    '--download-kokkos-kernels=git://https://github.com/kliegeois/kokkos-kernels',
    '--download-kokkos-kernels-commit=origin/develop',
    '--with-make-np=8',
#    '--with-hdf5-dir=/usr/common/software/sles15_cgpu/hdf5/1.10.5/gcc/8.3.0/',
#    'PETSC_ARCH=arch-perlmutter-opt-gcc-cuda'
  ]
  configure.petsc_configure(configure_options)
