#!/bin/bash -x
#SBATCH -A m3909_g
#SBATCH -C gpu
#SBATCH -J petsc-ex2
#SBATCH -q regular
#SBATCH -t 2:00:00
#SBATCH -N 1
#SBATCH -G 1

export SLURM_CPU_BIND="cores"

EXTRA='-log_view -dm_landau_verbose 1 -ts_view -options_left -log_view_gpu_time'

K_SOLV='-ksp_type preonly -pc_type bjkokkos -pc_bjkokkos_ksp_type tfqmr -pc_bjkokkos_pc_type jacobi -pc_bjkokkos_ksp_rtol 1.e-8 -pc_bjkokkos_ksp_max_it 100 -pc_bjkokkos_ksp_converged_reason -pc_bjkokkos_ksp_error_if_not_converged'
P_SOLV='-ksp_type tfqmr   -pc_type jacobi                                                                   -ksp_rtol     1.e-8             -ksp_max_it 100             -ksp_converged_reason '
LAND_2D='-dim 2 -dm_landau_amr_levels_max 2,2,2 -dm_landau_type p4est -petscspace_degree 3'
LAND_3D='-dim 3 -dm_landau_amr_levels_max 2,2,2 -dm_landau_type p8est -petscspace_degree 2'

NR=4

for BSZ3D in 1
do
    let "BSZ2D=${BSZ3D} * 8"
    BSTR2=$(printf "%03d" ${BSZ2D})
    BSTR3=$(printf "%03d" ${BSZ3D})

    #srun -G ${NR} -n ${NR} --cpu-bind=cores --ntasks-per-core=1 ./ex2-2 -dm_landau_batch_size ${BSZ2D} ${EXTRA} ${LAND_2D} ${K_SOLV} | tee out11_2D_${BSTR2}_Batch-Kokkos_TFQMR_Perlmutter_${NR}_NVIDIA-A100.txt
    #srun -G ${NR} -n ${NR} --cpu-bind=cores --ntasks-per-core=1 ./ex2-2 -dm_landau_batch_size ${BSZ2D} ${EXTRA} ${LAND_2D} ${P_SOLV} | tee out11_2D_${BSTR2}_Ensemble-PETSc_TFQMR_Perlmutter_${NR}_NVIDIA-A100.txt
    #srun -G ${NR} -n ${NR} --cpu-bind=cores --ntasks-per-core=1 ./ex2-3 -dm_landau_batch_size ${BSZ3D} ${EXTRA} ${LAND_3D} ${K_SOLV} | tee out11_3D_${BSTR3}_Kokkos_TFQMR_Perlmutter_${NR}_NVIDIA-A100.txt
    ./ex2 -dm_landau_batch_size ${BSZ2D} ${EXTRA} ${LAND_2D} ${P_SOLV} 
done

date
